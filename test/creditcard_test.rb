require 'test/unit'
require 'shoulda'
require_relative '../lib/credit_card'

class CreditCardTest < Test::Unit::TestCase
  context 'A new credit card' do

    context 'that is passed a valid card number' do
    
      setup do
        @amex_card = CreditCard.new('378282246310005')
        @amex_card_2 = CreditCard.new('340000000000611')
        @discover_card = CreditCard.new('6011111111111117')
        @mastercard_card = CreditCard.new('5555555555554444')
        @mastercard_card_2 = CreditCard.new('5105105105105100')
        @visa_card = CreditCard.new('4408041234567893')
      end

      should 'return the correct provider name' do
        assert_equal 'AMEX', @amex_card.provider
        assert_equal 'AMEX', @amex_card_2.provider
        assert_equal 'Discover', @discover_card.provider
        assert_equal 'MasterCard', @mastercard_card.provider
        assert_equal 'MasterCard', @mastercard_card_2.provider
        assert_equal 'Visa', @visa_card.provider
      end

      should 'return as a valid card number' do
        assert_true @amex_card.valid?
        assert_true @amex_card_2.valid?
        assert_true @discover_card.valid?
        assert_true @mastercard_card.valid?
        assert_true @mastercard_card_2.valid?
        assert_true @visa_card.valid?
      end

    end

    context 'that is passed an unknown card number' do

      setup do
        @unknown_card = CreditCard.new('123456789012345')
      end

      should 'return an uknown card provider name' do
        assert_equal 'Unknown', @unknown_card.provider
      end

    end

    context 'that is passed an invalid card number' do

      setup do
        @invalid_card_1 = CreditCard.new('4417123456789112')
        @invalid_card_2 = CreditCard.new('341234567890123')
        @invalid_card_3 = CreditCard.new('123456789012345')
      end

      should 'return as an invalid card number' do
        assert_false @invalid_card_1.valid?
        assert_false @invalid_card_2.valid?
        assert_false @invalid_card_3.valid?
      end

    end

  end
end