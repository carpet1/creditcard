#!/usr/bin/ruby
require_relative 'lib/credit_card'

card_number = ARGV.join.strip
abort ('Please enter a credit card number as an argument!') if card_number == ''
@card = CreditCard.new(card_number)
puts "#{ARGV.join(' ')} looks like a#{ @card.valid? ? ' ' : 'n in' }valid #{@card.provider} card"