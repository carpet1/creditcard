class CreditCard

  CARDS = {
    "AMEX" => { length: [15], begin: ['34', '37'] },
    "Discover" => { length: [16], begin: ['6011'] },
    "MasterCard" => { length: [16], begin: (51..55).map(&:to_s) },
    "Visa" => { length: [13, 16], begin: ['4'] }
  }

  def initialize(number)
    @number = number
  end

  def provider
    CARDS.each do |provider, patterns|
      return provider if patterns[:length].any? { @number.length } && patterns[:begin].any? { |b| @number =~ /^#{b}/ }
    end
    return 'Unknown'
  end

  def valid?
    str = ''
    # Reverse card number and iterate over digits, doubling every other digit appending to str var
    @number.reverse.each_char.with_index(0) { |c, i| str << (i.even? ? c : (c.to_i * 2).to_s) }
    # Sum all the digits together and check it's a multiple of 10
    str.split('').map(&:to_i).inject { |sum, n| sum + n } % 10 == 0
  end
end